# Create the S3 bucket policy
resource "aws_iam_policy" "s3_policy_allow_all" {
  name        = "s3_policy_allow_all"
  description = "Allows all permissions to this S3 bucket"

  policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
	  {
		"Action": "s3:*",
		"Effect": "Allow",
		"Resource": [
		"arn:aws:s3:::var.s3_terraform_bucket",
		"arn:aws:s3:::var.s3_terraform_bucket/*"
		]
	  }
	]
}
EOF
}

# Create profile with role
resource "aws_iam_instance_profile" "S3_profile_allow_all" {
  name = "S3_profile_allow_all"
  role = aws_iam_role.s3_role_allow_all.name
}

# Create role
resource "aws_iam_role" "s3_role_allow_all" {
  name = "s3_role_allow_all"

  assume_role_policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
	  {
		"Action": "sts:AssumeRole",
		"Principal": {
		  "Service": "ec2.amazonaws.com"
		},
		"Effect": "Allow",
		"Sid": ""
	  }
	]
}
EOF
}

# Attach role to policy
resource "aws_iam_role_policy_attachment" "attach_role" {
  role       = aws_iam_role.s3_role_allow_all.name
  policy_arn = aws_iam_policy.s3_policy_allow_all.arn
}