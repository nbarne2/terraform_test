# Create a new instance of the latest Ubuntu 20.04 on a
# t2.micro node (Free eligible)

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = [var.ami_flavor]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_key_pair" "ubuntu" {
  key_name   = "ubuntu"
  public_key = file("tutorial.pem")
}

resource "aws_instance" "ubuntu" {
  ami                  = data.aws_ami.ubuntu.id
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.S3_profile_allow_all.name
  security_groups      = [aws_security_group.ingress_allow_ssh.name]
  key_name             = aws_key_pair.ubuntu.key_name

  tags = {
    Name          = "TerraformEC2",
    ProductCode   = var.product_code_tag,
    InventoryCode = var.inventory_code_tag
  }


}