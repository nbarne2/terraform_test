provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

locals {
  standard_tags = {
    ProductCode   = var.product_code_tag
    InventoryCode = var.inventory_code_tag
  }
}