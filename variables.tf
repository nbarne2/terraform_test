# Terraform variables
variable "aws_profile" {
  type        = string
  description = "Name of the AWS profile to which deployment is done"
  default     = "default"
}
variable "aws_region" {
  type        = string
  description = "Code of the region to which deployment is done, eg: us-west-2"
  default     = "us-west-2"
}
variable "product_code_tag" {
  type        = string
  description = "Product code of the environment for accounting and governance purposes"
  default     = "PRD2318"
}
variable "inventory_code_tag" {
  type        = string
  description = "InventoryCode tag attached to resource"
  default     = "Noah_Prod_S3"
}
variable "s3_bucket" {
  type        = string
  description = "S3 bucket name"
  default     = "bucket5679"
}
variable "instance_type" {
  type        = string
  description = "Instance type, eg: t3.medium"
  default     = "t2.micro"
}
variable "ami_flavor" {
  type        = string
  description = "Instance type, eg: t3.medium"
  default     = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}
variable "environment" {
  type        = string
  description = "Which environment such as staging or prod"
  default     = "prod"
}