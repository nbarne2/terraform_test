d# Terraform project

This project uses Terraform to create the following resources in AWS:  
- An S3 bucket
- An IAM role  
- An IAM policy attached to the role that allows it to perform any S3 actions on that bucket and the objects in it 
- An EC2 instance with the IAM role attached and SSH inbound port open
 
## Local prerequisites
Install AWS CLI locally and run 
```bash
$ aws configure
```
This will prompt you to answer some questions.

Terraform will need to authenticate to AWS, so an IAM user will need to be created for Terraform to run as.
Terraform will use the AWS environment variables stored at `~/.aws/credentials`

Link to [AWS Configuration and credential file settings](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)

## Environment creation

The `env=prod` command allows easy switching between environments such as staging and prod.
The `-auto-approve` switch assumes yes and doesn't ask the question "Do you want to perform these actions?"

Install Terraform v0.13.2 and run these:  
```bash
$ env=prod
$ terraform fmt
$ terraform init
$ terraform plan -var-file=config/$env.tfvars
$ terraform apply -var-file=config/$env.tfvars -auto-approve
```
`Warning` - Be sure to verify that upgrading to Terraform v0.13.2 will not break any older scripts

## Notes on S3 bucket

- `naming` - The s3 bucket name has to be globally unique.
- `deletion` - Be sure that the bucket is empty prior to destruction

```
It is a best practice to use bucket names that contain your domain name and conform to the rules for DNS names.
This ensures that your bucket names are your own, can be used in all regions, and can host static websites.
```

## Testing

Generate a key pair locally.
```bash
$ ssh-keygen -t rsa
Enter file in which to save the key (/home/dev/.ssh/id_rsa): key
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in key.
Your public key has been saved in key.pub.
```

Now setup permissions on those files:
```bash
$ chmod 400 key*
```

Login to the EC2 instance and validate that you’re able to access the bucket using the role.
You can login using the output from the apply command. Make sure that the key file is located in the same directory or set the path to the file after -i below.
```bash
$ ssh -i "key" ubuntu@[ec2-public-ip or ec2-public-dns]
```
 
Alternatively, you can check IAM in the console or from CLI to ensure the policy/role is set up as desired.
 
## Environment destruction
 
Run this when you are ready to tear everything down.
```bash
$ terraform destroy -var-file=config/$env.tfvars -auto-approve
```
