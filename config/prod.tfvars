# Set variable values declared in variables.tf
aws_profile         = "default"
aws_region          = "us-west-2"
product_code_tag    = "PRD2318"
inventory_code_tag  = "Noah_Prod_S3"
s3_bucket           = "tf-bucket5679"
instance_type       = "t2.micro"
ami_flavor          = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
environment         = "prod"